package ro.iss.smart.academy.model;

import java.util.Date;

public class EmployeeFactory {

    public static Employee createEmployee(EmployeeType employeeType, String firstName, String lastName, Date hiringDate){
        Employee employee;

        switch (employeeType) {
            case CHIEF:
                employee = new Chief();
                break;
            case COURIER:
                employee = new Courier();
                break;
            case MANAGER:
                employee = new Manager();
                break;
            default:
                employee = new Operator();
                break;

        }

        employee.setFirstName(firstName);
        employee.setLastName(lastName);
        employee.setHiringDate(hiringDate);

        return employee;
    }
}
