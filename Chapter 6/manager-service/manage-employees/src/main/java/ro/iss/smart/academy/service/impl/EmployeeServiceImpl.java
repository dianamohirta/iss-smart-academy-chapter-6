package ro.iss.smart.academy.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.iss.smart.academy.dto.EmployeeDTO;
import ro.iss.smart.academy.dto.OrderDTO;
import ro.iss.smart.academy.model.Employee;
import ro.iss.smart.academy.service.EmployeeService;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

    List<EmployeeDTO> employees = new ArrayList<>();

    @Override
    public List<EmployeeDTO> getEmployees(boolean isActive) {
        return employees.stream().filter( employeeDTO -> employeeDTO.);
    }

    @Override
    public Employee createEmployee(EmployeeDTO employeeDTO) {
        return null;
    }

    @Override
    public void updateEmployee(int id) {

    }

    @Override
    public void deleteEmployee(int id) {

    }

    @Override
    public void assignOrder(OrderDTO orderDTO) {

    }
}
