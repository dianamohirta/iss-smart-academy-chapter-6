package ro.iss.smart.academy.state;

public enum WorkingState {
    VACATION, BREAK, ACTIVE
}
