package ro.iss.smart.academy.model;

public enum EmployeeType {
    CHIEF, COURIER, MANAGER, OPERATOR
}
