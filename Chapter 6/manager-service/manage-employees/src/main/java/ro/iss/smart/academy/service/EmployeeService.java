package ro.iss.smart.academy.service;

import ro.iss.smart.academy.dto.EmployeeDTO;
import ro.iss.smart.academy.dto.OrderDTO;
import ro.iss.smart.academy.model.Employee;

import java.util.List;

public interface EmployeeService {

    List<EmployeeDTO> getEmployees(boolean isActive);

    Employee createEmployee(EmployeeDTO employeeDTO);

    void updateEmployee(int id);

    void deleteEmployee(int id);

    void assignOrder(OrderDTO orderDTO);
}
