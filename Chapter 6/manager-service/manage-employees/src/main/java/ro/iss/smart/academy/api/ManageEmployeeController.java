package ro.iss.smart.academy.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.iss.smart.academy.dto.EmployeeDTO;
import ro.iss.smart.academy.model.Employee;
import ro.iss.smart.academy.model.EmployeeFactory;
import ro.iss.smart.academy.model.EmployeeType;

@RestController
@ControllerAdvice
@Slf4j
@RequestMapping("/v1/employee")
public class ManageEmployeeController {

    @PostMapping(
            path = "/create",
            consumes = "application/json",
            produces = "application/json"
    )
    public Employee createEmployee(EmployeeDTO employeeDTO) {
        log.info("CREATE EMPLOYEE");
        return EmployeeFactory.createEmployee(EmployeeType.valueOf(employeeDTO.getEmployeeType()), employeeDTO.getFirstName(), employeeDTO.getLastName(), null);
    }
}
