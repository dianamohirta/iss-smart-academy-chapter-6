package ro.iss.smart.academy.dto;

import lombok.Data;
import ro.iss.smart.academy.state.BreakState;
import ro.iss.smart.academy.state.State;
import ro.iss.smart.academy.state.VacationState;
import ro.iss.smart.academy.state.WorkingState;

import java.util.Date;

@Data
public class EmployeeDTO {

    private String firstName;
    private String lastName;
    private Date hiringDate;
    private String employeeType;
    private State state;

    private WorkingState getState(){

        if(state instanceof BreakState)
            return WorkingState.BREAK;

        else if(state instanceof VacationState)
            return WorkingState.VACATION;

        else return WorkingState.ACTIVE;
    }
}
