package ro.iss.smart.academy.model;

import lombok.Data;
import ro.iss.smart.academy.state.State;

import java.util.Date;

@Data
public abstract class Employee {

    private int id;
    private String firstName;
    private String lastName;
    private Date hiringDate;

    private State state;

    public abstract void work();
    public abstract void obtainDetails();
//    public abstract void process(OrderDTO orderDTO);
}
