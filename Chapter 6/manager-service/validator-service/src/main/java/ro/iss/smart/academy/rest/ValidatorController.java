//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package ro.iss.smart.academy.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ro.iss.smart.academy.dto.CnpDTO;
import ro.iss.smart.academy.dto.IbanDTO;
import ro.iss.smart.academy.service.CnpService;
import ro.iss.smart.academy.service.IbanService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RequestMapping({"/v1/validate"})
@RestController
@ControllerAdvice
@Slf4j
public class ValidatorController {

    private CnpService cnpService;
    private IbanService ibanService;

    public ValidatorController(CnpService cnpService, IbanService ibanService){
        this.cnpService = cnpService;
        this.ibanService = ibanService;
    }

    @GetMapping(
            value = {"/cnp"},
            produces = {"application/json"}
    )
    @ResponseStatus(HttpStatus.OK)
    public CnpDTO checkCnp(@RequestParam(value = "cnp") String cnp, HttpServletRequest var2, HttpServletResponse var3) {

        log.info("Check cnp for: {}", cnp);
        return cnpService.verify(cnp);
    }
    @GetMapping(
            value = {"/iban"},
            produces = {"application/json"}
    )
    @ResponseStatus(HttpStatus.OK)
    public IbanDTO checkIban(@RequestParam(value = "iban") String iban, HttpServletRequest var2, HttpServletResponse var3) {

        log.info("Check iban for: {}", iban);
        return ibanService.checkValidityForIban(iban);
    }

}
