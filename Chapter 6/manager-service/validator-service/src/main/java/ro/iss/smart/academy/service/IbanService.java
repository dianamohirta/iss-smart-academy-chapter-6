package ro.iss.smart.academy.service;

import ro.iss.smart.academy.dto.IbanDTO;

public interface IbanService {
    IbanDTO checkValidityForIban(String iban);
}
