package ro.iss.smart.academy.dto;

import lombok.Data;

@Data
public class IbanDTO {

    private boolean isRo;
    private boolean isTrezo;
    private boolean isValid;
}
