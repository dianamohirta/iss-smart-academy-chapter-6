package ro.iss.smart.academy.service;

import ro.iss.smart.academy.dto.CnpDTO;

public interface CnpService {

    CnpDTO verify(String cnp);
}
