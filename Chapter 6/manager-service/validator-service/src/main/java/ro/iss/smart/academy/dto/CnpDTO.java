package ro.iss.smart.academy.dto;

import lombok.Data;

@Data
public class CnpDTO {
    private boolean isCorrect;
    private boolean isUnderAge;
    private boolean isForeigner;
    private Sex sex;

    public enum Sex {
        M("M"),
        F("F");

        private String value;
        Sex(String value){
            this.value = value;
        }
    }
}
