package ro.iss.smart.academy.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.iss.smart.academy.dto.CnpDTO;
import ro.iss.smart.academy.service.CnpService;

import java.util.Calendar;
import java.util.Date;

@Service
@Slf4j
public class CnpServiceImpl implements CnpService {

    @Override
    public CnpDTO verify(String cnp) {
        boolean isCorrect = functionvalidCnp(cnp);
        CnpDTO cnpDTO = new CnpDTO();
        cnpDTO.setCorrect(isCorrect);

        if(isCorrect){
            cnpDTO.setSex(retrieveSex(cnp));
            cnpDTO.setUnderAge(isUnderAge(cnp));
            cnpDTO.setForeigner(isForeigner(cnp));
        }

        return cnpDTO;
    }

    public CnpDTO.Sex retrieveSex(String cnp){
        if(cnp.startsWith("2") || cnp.startsWith("4") || cnp.startsWith("6") || cnp.startsWith("8")){
            return CnpDTO.Sex.F;
        } else return CnpDTO.Sex.M;
    }

    public boolean isForeigner(String cnp){
        if(cnp.substring(0,1).equals("7")){
            return true;
        }else if(cnp.substring(0,1).equals("8")){
            return true;
        }
        else {
            return false;
        }
    }

    public Date extractDateOfBirth(String cnp){
        return null;
    }

    public boolean isUnderAge(String cnp) {

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        int birthDay = Integer.parseInt(cnp.substring(5, 7));
        int birthMonth = Integer.parseInt(cnp.substring(3, 5));
        int birthYear;

        if(cnp.startsWith("5") || cnp.startsWith("6")) {
            birthYear = 2000 + Integer.parseInt(cnp.substring(1, 3));
        } else {
            birthYear = 1900 + Integer.parseInt(cnp.substring(1, 3));
        }

        if(birthYear == year - 18){

            if(birthMonth < month){
                return false;
            } else if (birthMonth == month){
                if(birthDay <= day)
                    return true;
            } else return true;
        }
        else if(birthYear < year - 18){
            return false;
        }
        else {
            return true;
        }

        return false;
    }

    public boolean functionvalidCnp(String cnp) {

        if (cnp.length()!=13) {
            return false;
        }
        if (!cnp.matches("^[0-9]+$")) {
            return false;
        } else {
            if (cnp.length()>10&&cnp.length()<13) {
                return false;
            }

            if (cnp.length()==13) {
                if (!isCnpOK(cnp)) {
                    return false;
                }
            }
        }
        return true;
    }


    public boolean isCnpOK(String cnp) {

        String[] cnpArray = cnp.split("");
        Integer[] controlArray = {2,7,9,1,4,6,3,5,8,2,7,9};
        Integer sum = 0;

        for (int i = 0 ; i < cnpArray.length - 1; i++ ){
            sum = sum + (Integer.valueOf(cnpArray[i]) * controlArray[i]) ;
        }

        int remainder = sum % 11;

        if (remainder==10) {
            remainder=1;
        }

        int month = 10*Integer.valueOf(cnpArray[3])+Integer.valueOf(cnpArray[4]);
        int day = 10*Integer.valueOf(cnpArray[5])+Integer.valueOf(cnpArray[6]);

        if (month>12 || day>31 ||
                Integer.valueOf(cnpArray[12]) != remainder) {
            return false;
        }

        return true;
    }
}
