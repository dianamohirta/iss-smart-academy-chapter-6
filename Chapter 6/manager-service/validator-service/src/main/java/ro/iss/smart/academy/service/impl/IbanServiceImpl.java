package ro.iss.smart.academy.service.impl;

import org.springframework.stereotype.Service;
import ro.iss.smart.academy.dto.IbanDTO;
import ro.iss.smart.academy.service.IbanService;

@Service
public class IbanServiceImpl implements IbanService {
    @Override
    public IbanDTO checkValidityForIban(String iban) {
        boolean is_correct = is_correct(iban);
        IbanDTO ibanDTO = new IbanDTO();
        ibanDTO.setValid(is_correct);
        if(is_correct){
            ibanDTO.setRo(verifyIfIsRo(iban));
            ibanDTO.setTrezo(verifyIfTrezo(iban));
        }

        return ibanDTO;
    }

    public boolean verifyIfIsRo(String iban){
        return iban.substring(0,2).equals("RO");
    }

    public boolean is_correct(String iban){
        return iban.length() >= 18 && iban.length() < 20;
    }

    public boolean verifyIfTrezo(String iban){
        return iban.contains("TREZO");
    }



}
